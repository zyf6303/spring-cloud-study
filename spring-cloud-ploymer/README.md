![image](https://img.shields.io/badge/Spring%20Cloud-%E2%98%85%E2%98%85%E2%98%85-green.svg)
![image](https://img.shields.io/badge/Netflix-%E2%98%85%E2%98%85%E2%98%85-red.svg)

spring-cloud-study 微服务组件学习
===

http://blog.csdn.net/moshowgame

<table>
<tbody><tr>
<td>工程名</td>  <td>描述</td>  <td>端口</td>
</tr>
<tr>
<td>spring-cloud-ploymer-center</td>  <td>服务发现与注册中心</td> 
</tr>
<tr>
<td>spring-cloud-ploymer-router</td>  <td>动态转发路由器</td>  
</tr>
<tr>
<td>spring-cloud-ploymer-demo</td>  <td>配置管理中心</td> 
</tr>
<tr>
<td>spring-cloud-study-configcenter</td>  <td>配置中心</td>  
</tr>
<tr>
<td>spring-cloud-ploymer-oaurth(待添加)</td>  <td>TOKEN认证中心</td> 
</tr>
</tbody></table>

环境：JDK1.8(8~10都可以)
编码：UTF-8

```
 <properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <java.version>1.8</java.version>
  </properties>
```

有关项目启动和配置的说明：

1、最先启动的是spring-cloud-study-eureka，因为它是注册中心，大多数微服务必须依赖于它才能实现必要的功能。 <br>
2、接着zuul路由中心，启用spring-cloud-study-zuul，并配置yml文件即可(已经带了一点小配置，可根据实际情况修改)。 <br>
3、然后启用pring-cloud-study-demo，这是一个demo项目<br>



端口启动情况：
【eureka】
http://127.0.0.1:8080/eureka 注册中心
【zuul】
http://127.0.0.1:8081/api1/demo/index 路由转发请求
http://127.0.0.1:8081/api2/demo/index
http://127.0.0.1:8081/api1/demo/socket/222
【demo】
http://127.0.0.1:8082/demo/socket/222  websocket请求页面
http://127.0.0.1:8082/demo/index json数据返回
http://127.0.0.1:8082/demosocket/222  socket请求地址
http://127.0.0.1:8082/basepath 获取微服务路径
【configcenter】
http://127.0.0.1:5555/getparam 获取变量<br>

